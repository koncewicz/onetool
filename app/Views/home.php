<?= $this->extend($config->viewLayout) ?>
<?= $this->section('main') ?>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?=lang('OneTool.welcome')?></div>

                <div class="card-body">
                    You are in home page (only for the login users).
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>
