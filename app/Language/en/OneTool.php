<?php

return [
    'emailAddress' => 'E-Mail Address',
    'username' => 'Username',
    'register'     => 'Register',
    'password'     => 'Password',
    'confirmPassword' => 'Confirm Password',
    'recaptchaError' => 'Recaptcha is not valid.',
    'debounceError' => 'Please use real E-Mail Address.',
    'welcome' => 'Welcome!'
];