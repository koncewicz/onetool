<?php namespace App\Validation;

use Config\OneTool;

class OneToolRules
{
    /**
     * @var OneTool
     */
    protected $config;

    /**
     * OneToolController constructor.
     */
    public function __construct()
    {
        $this->config = config('OneTool');
    }

    public function recaptcha(string $str, string &$error = null): bool
    {
        $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . $this->config->recaptchaSecretKey . '&response=' . $str);
        $responseData = json_decode($verifyResponse);
        if (!$responseData->success) {
            $error = lang('OneTool.recaptchaError');
            return false;
        }

        return true;
    }

    public function debounce(string $str, string &$error = null): bool
    {
        $data = file_get_contents('https://api.debounce.io/v1/?api=' . $this->config->debounceApiKey . '&email=' . $str);
        $responseData = json_decode($data);

        if (!in_array($responseData->debounce->code, [4, 5, 8])) {
            $error = lang('OneTool.debounceError');
            return false;
        }

        return true;
    }
}