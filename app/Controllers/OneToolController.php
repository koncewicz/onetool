<?php namespace App\Controllers;

use Config\OneTool;

class OneToolController extends BaseController
{
    /**
     * @var OneTool
     */
    protected $config;

    /**
     * OneToolController constructor.
     */
    public function __construct()
    {
        $this->config = config('OneTool');
    }

    /**
     * @return string
     */
    public function home()
	{
	    if (!session()->has('login_user_id')) {
            return redirect()->route('registerView');
        }

		return view('home', ['config' => $this->config]);
	}

    /**
     * @return string
     */
	public function registerView()
    {
        if (session()->has('login_user_id')) {
            return redirect()->route('home');
        }

        return view('register', ['config' => $this->config]);
    }

    /**
     * @return string
     */
	public function register()
    {
        if (session()->has('login_user_id')) {
            return redirect()->route('home');
        }

        $rules = [
            'email'    => 'required|valid_email|is_unique[users.email]|debounce',
            'username' => 'required|is_unique[users.username]',
            'g-recaptcha-response' => 'required|recaptcha'
        ];

        if (!$this->validate($rules)) {
            return redirect()->back()->withInput()->with('errors', service('validation')->getErrors());
        }

        $user = model('UserModel');
        $data = [
            'email' => $this->request->getPost('email'),
            'username' => $this->request->getPost('username')
        ];
        $id = $user->insert($data);

        session()->set('login_user_id', $id);

        return redirect()->route('home');
    }
}
