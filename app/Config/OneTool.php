<?php namespace Config;

use CodeIgniter\Config\BaseConfig;

class OneTool extends BaseConfig
{
    public $viewLayout = 'Views\layout';

    public $recaptchaSecretKey = '';
    public $debounceApiKey = '';
}