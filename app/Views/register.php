<?= $this->extend($config->viewLayout) ?>
<?= $this->section('main') ?>

<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><?=lang('OneTool.register')?></div>

                <div class="card-body">
                    <form action="<?= route_to('register') ?>" method="post">
                        <?= csrf_field() ?>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"><?=lang('OneTool.emailAddress')?></label>

                            <div class="col-md-6">
                                <input type="email" class="form-control <?php if(session('errors.email')) : ?>is-invalid<?php endif ?>"
                                       name="email" aria-describedby="emailHelp" placeholder="<?=lang('OneTool.emailAddress')?>" value="<?= old('email') ?>">

                                <?php if(session('errors.email')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?=session('errors.email')?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right"><?=lang('OneTool.username')?></label>

                            <div class="col-md-6">
                                <input type="text" class="form-control <?php if(session('errors.username')) : ?>is-invalid<?php endif ?>"
                                       name="username" placeholder="<?=lang('OneTool.username')?>" value="<?= old('username') ?>">

                                <?php if(session('errors.username')): ?>
                                    <span class="invalid-feedback" role="alert">
                                        <strong><?=session('errors.username')?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-md-4 col-form-label text-md-right"></label>

                            <div class="col-md-6">
                                <div class="g-recaptcha" data-expired-callback="expiredCallback" data-callback="submitCallback" data-sitekey="6Ld6HPsUAAAAADkgyTjfTN00LfcIDyBDeVxT1zyF">
                                </div>

                                <?php if(session('errors.g-recaptcha-response')): ?>
                                    <span class="invalid-feedback d-block" >
                                        <strong><?=session('errors.g-recaptcha-response')?></strong>
                                    </span>
                                <?php endif; ?>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" disabled class="btn btn-primary">
                                    <?=lang('OneTool.register')?>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var submitCallback = function() {
        $('button[type=submit]').removeAttr("disabled");
    };

    var expiredCallback = function() {
        $('button[type=submit]').prop("disabled", true);
    };
</script>

<?= $this->endSection() ?>
